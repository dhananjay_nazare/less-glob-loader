const loaderUtils = require('loader-utils');
const glob = require('glob');

module.exports = function ExpandLess(source, options) {
	// Default nodir to true
	options.nodir = typeof options.nodir !== 'undefined' ? options.nodir : true;

	let { test = "import", delimiter = '\n', exclude = null, prefix = '///' } = options;
	const qualifier = new RegExp(`^.*\\b${test}\\b(.*)$`, 'gm');

	function expandGlob(result) {
		if (!result) return;
		const [match, quote, content] = result;
		const offset = result.index;
		const line = result.input;

		if (!glob.hasMagic(content)) return;

		let pre = line.slice(0, offset),
			post = line.slice(offset + match.length);

		if(pre &&  pre.indexOf(prefix) == 0 ){
			pre = pre.replace(prefix, '');
		}

		let results = glob.sync(content, options);
		if(exclude){
			var excludePattern = new RegExp(exclude);
			results = results.filter(filename => !excludePattern.test(filename));
		}
		return results.map(filename => `${pre}${quote}${filename}${quote}${post}`).join(delimiter);
	}

	const quotedString = /(['"])(.*?)\1/;
	function expandLine(line, payload) {
		if (!(payload && payload.trim())) return line;
		return expandGlob(quotedString.exec(line)) || line;
	}

	return source.replace(qualifier, expandLine);
}
