let expandLess = require('./lib/expandLess');

class LessGlob{
	constructor(options){
	    this.options = options || {};
	}

	process(src, extra) {
	    // skip if it's not a less file
	    if (extra.fileInfo && !/\.less/i.test(extra.fileInfo.filename)) {
	      return src;
	    }

	    this.options.cwd = extra.fileInfo.currentDirectory;
	    // process file
	    return expandLess(src, this.options);
  	}	
}

class LessGlobPlugin {
	constructor(options){
	    this.options = options || {};
	}

	install(less, pluginManager){
	    pluginManager.addPreProcessor(new LessGlob(this.options), 2000);
  	}
}

module.exports = LessGlobPlugin;



