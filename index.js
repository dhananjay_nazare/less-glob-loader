const loaderUtils = require('loader-utils');
const expandLess = require('./lib/expandLess');

module.exports = function importGlob(source) {
	this.cacheable(false);
	const options = loaderUtils.getOptions(this);
	options.cwd = this.context;
	return expandLess(source, options);
}
